module.exports = {
  mode: 'jit',
  purge: [
    './public/**/*.html'
  ],

  darkMode: false, // or 'media' or 'class'


  theme: {
    extend: {

      screens: {
        '3xl': '1600px',
      },

      fontSize: {
  
        'title': '4rem', //  64px    - Large Titles
        'smallerTitle': '1.6rem', //  27px  - Smaller Titles
        'mediumTitle': '2.125rem', //  34px  - Smaller Titles
        'contentTitle':  '2.75rem', // 44px - Text titles
        'lg':  '1.438rem', // 24px - Product Prices
        'xxs': '1.063rem', //  17px    -
        '2xs': '0.938rem', //  13px    -
        '3xs': '0.813rem', //  13px    -
        '4xs': '0.688rem', //  13px    - Smaller banner text
        '5xs': '0.438rem', //  7px    - Basket

      },



      lineHeight: {


      },

      letterSpacing: {


      },



      colors: {
        primary: '#002959', // Blue
        lighterBlue: '#0053B4', // Lighter Blue
        secondary: '#FFFFFF', // White  
        warmGrey: '#F3F4F5', // Warm Grey  
        fadedGrey: '#B3BFCD', // Faded Grey  
        mediumGrey: '#8094ac', // Medium Grey  
        darkGrey: '#405f82', // Dark Grey  
        fadeOrange: '#FF8143', // Orange
        darkOrange: '#E55F1C', // DarkOrange
        rustRed: '#BC3E23', // Red


      },

      spacing: {
        '1.25': '0.3125rem', //5px          
        '2.5': '0.625rem', //10px 
        '2.75': '0.6875rem', //11px 
        '5.25': '1.3125rem', //21px           
        '7.5': '1.875rem', //30px       
        '12.5': '3.125rem', //50px
        '13.5': '3.375rem', //54px
        '15': '3.75rem', //60px 
        '15.5': '3.875rem', //62px           
        '17.5': '4.375rem', //70px 
        '22.5': '5.625rem', //90px 
        '25': '6.25rem', //100px  
        '33.75': '8.4375rem', //135px 
        '30': '7.5rem', //120px
        '50': '12.5rem', //200px 
        '78': '19.5rem', //312px 
        '95': '23.75rem', //380px 
        '200': '50rem', //800px    


      },

      maxWidth: {

      }
    },




    fontFamily: {
      'neuzeit': ['neuzeit-grotesk, sans-serif'],
    },

  },
  variants: {
    extend: {
      scale: ['active', 'group-hover'],
      textColor: ['group-hover'],
      borderRadius: ['hover', 'focus'],
      mixBlendMode: ['hover', 'focus'],
      translate: ['hover', 'group-hover'],
      opacity: ['hover', 'group-hover'],
      transform: ['hover', 'group-hover'],

    },
  },
  plugins: [],
}